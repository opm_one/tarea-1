package com.crypton.tareamito.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crypton.tareamito.dao.IProductoDao;
import com.crypton.tareamito.model.Producto;
import com.crypton.tareamito.service.IProductoService;

@Service
public class IProductoServiceImpl implements IProductoService{

	@Autowired
	IProductoDao dao;
	
	@Override
	public Producto registrar(Producto t) {
		return dao.save(t);
	}

	@Override
	public void modificar(Producto t) {
		dao.save(t);
		
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
		
	}

	@Override
	public Producto listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
