package com.crypton.tareamito.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crypton.tareamito.dao.IPersonaDao;
import com.crypton.tareamito.model.Persona;
import com.crypton.tareamito.service.IPersonaService;

@Service
public class IPersonaServiceImpl implements IPersonaService{

	@Autowired
	IPersonaDao dao;

	@Override
	public Persona registrar(Persona t) {
		// TODO Auto-generated method stub
		return dao.save(t);
	}
	@Override
	public void modificar(Persona t) {
		dao.save(t);
		
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
		
	}

	@Override
	public Persona listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Persona> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}
}
