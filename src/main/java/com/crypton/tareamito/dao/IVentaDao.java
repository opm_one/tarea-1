package com.crypton.tareamito.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crypton.tareamito.model.Venta;

@Repository
public interface IVentaDao extends JpaRepository<Venta, Integer>{

}
