package com.crypton.tareamito.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crypton.tareamito.dao.IVentaDao;
import com.crypton.tareamito.model.Venta;
import com.crypton.tareamito.service.IVentaService;

@Service
public class IVentaServiceImpl implements IVentaService {

	@Autowired
	IVentaDao dao;
	
	@Override
	public Venta registrar(Venta t) {
		// TODO Auto-generated method stub
		t.getListaDetalle().forEach(y -> y.setVenta(t));
		return dao.save(t);
	}

	@Override
	public void modificar(Venta t) {		
	}

	@Override
	public void eliminar(int id) {
		
	}

	@Override
	public Venta listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	
}
