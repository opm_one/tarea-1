package com.crypton.tareamito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareaMitoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TareaMitoApplication.class, args);
	}
}
