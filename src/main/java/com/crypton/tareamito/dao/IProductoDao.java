package com.crypton.tareamito.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crypton.tareamito.model.Producto;

@Repository
public interface IProductoDao extends JpaRepository<Producto, Integer> {

}
