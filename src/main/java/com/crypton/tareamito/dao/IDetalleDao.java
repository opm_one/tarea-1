package com.crypton.tareamito.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crypton.tareamito.model.DetalleVenta;

@Repository
public interface IDetalleDao extends JpaRepository<DetalleVenta, Integer>{

}
