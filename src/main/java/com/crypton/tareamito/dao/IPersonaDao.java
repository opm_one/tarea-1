package com.crypton.tareamito.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crypton.tareamito.model.Persona;

@Repository
public interface IPersonaDao extends JpaRepository<Persona	, Integer>{

}
